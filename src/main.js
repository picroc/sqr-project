import App from './App.svelte';
import 'Source/assets/global.css';
import 'Source/assets/global.scss';

const app = new App({
	target: document.body,
});
window.app = app;

export default app;
