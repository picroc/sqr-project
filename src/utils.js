const pad = num => ('0' + num).slice(-2);

export const formatDate = date => {
	const yyyy = date.getUTCFullYear();
	const mm = date.getUTCMonth() + 1;
	const dd = date.getUTCDate();
	return `${yyyy}-${pad(mm)}-${pad(dd)}`;
};

export const formatTime = date => {
	const hh = date.getHours();
	const mm = date.getMinutes();
	return `${pad(hh)}:${pad(mm)}`;
};
