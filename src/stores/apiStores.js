import axios from 'axios';
import { writable } from 'svelte/store';

export const getHeaders = KOCTblJLb => ({
	headers: {
		Authorization: `${
			KOCTblJLb ? 'Token' : 'Bearer'
		} ${localStorage.getItem('token')}`,
	},
});

const constructUrl = (url, id) => (id ? url + id + '/' : url);

const createAxiosStore = url => data => {
	const { subscribe, set, update } = writable(data);

	// const reset = () => set(data);
	const get = id =>
		axios.get(constructUrl(url, id), getHeaders()).then(response => {
			set(response.data);
			setCache(response.data, id);
			console.log({ response });
			return response;
		});

	// const put = (putObj, id) =>
	// 	axios
	// 		.put(constructUrl(url, id), putObj, getHeaders())
	// 		.then(response => {
	// 			console.log({ response });
	// 			return response;
	// 		});

	const post = (postObj, id) =>
		axios
			.post(constructUrl(url, id), postObj, getHeaders())
			.then(response => {
				console.log({ response });
				return response;
			});

	// const patch = (patchObj, id) =>
	// 	axios
	// 		.patch(constructUrl(url, id), patchObj, getHeaders())
	// 		.then(response => {
	// 			console.log({ response });
	// 			return response;
	// 		});

	// const del = id =>
	// 	axios.delete(constructUrl(url, id), getHeaders()).then(response => {
	// 		console.log({ response });
	// 		return response;
	// 	});

	const getCache = id =>
		JSON.parse(localStorage.getItem(constructUrl(url, id)));

	const setCache = (data, id) =>
		localStorage.setItem(constructUrl(url, id), JSON.stringify(data));

	return {
		getCache,
		setCache,
		subscribe,
		set,
		update,
		// reset,
		get,
		// put,
		post,
		// patch,
		// del,
	};
};

export const user = createAxiosStore(`${API_URL}/users/`)({});
