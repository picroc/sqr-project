const sveltePreprocess = require('svelte-preprocess');
const makeAttractionsImporter = require('attractions/importer.js');

const scssAliases = aliases => {
	return (url, _prev) => {
		for (const [alias, aliasPath] of Object.entries(aliases)) {
			if (url.indexOf(alias) === 0) {
				return {
					file: url.replace(alias, aliasPath),
				};
			}
		}
		return makeAttractionsImporter({
			themeFile: 'src/assets/theme.scss',
		})(url, _prev);
	};
};

module.exports.preprocess = sveltePreprocess({
	postcss: {
		plugins: [require('autoprefixer')],
	},
	scss: {
		importer: [
			scssAliases({
				styles: process.cwd() + '/src/assets',
			}),
		],
		includePaths: [process.cwd() + '/src/assets'],
	},
});
