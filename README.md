# SQR Project : HackTheBus

## Get started

Install the dependencies...

```bash
cd svelte-app
yarn
```

...then start webpack:

```bash
yarn start
```

Navigate to [localhost:8080](http://localhost:8080). You should see your app running. Edit a component file in `src`, save it, and the page should reload with your changes.

