const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const { preprocess } = require('./svelte.config.js');
const merge = require('webpack-merge');
const webpack = require('webpack');

const mode = process.env.NODE_ENV || 'development';

const common = {
	entry: {
		index: path.join(__dirname, 'src') + '/main.js',
	},
	resolve: {
		alias: {
			Source: path.resolve(__dirname, 'src'),
			svelte: path.resolve('node_modules', 'svelte'),
			images: path.resolve(__dirname, 'src/assets/images'),
			inline: path.resolve(__dirname, 'src/assets/inline'),
			styles: path.resolve(__dirname, 'src/assets'),
		},
		extensions: ['.mjs', '.js', '.json', '.svelte', '.html'],
		mainFields: ['svelte', 'browser', 'module', 'main'],
	},
	output: {
		filename: '[name].bundle.js',
		path: path.join(__dirname, 'build'),
	},
	optimization: {
		runtimeChunk: 'single',
		splitChunks: {
			chunks: 'all',
		},
	},
	mode,
	module: {
		rules: [
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					'style-loader',
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: ['./node_modules'],
							},
						},
					},
				],
			},
			{
				test: /attractions\/index\.js$/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env'],
					},
				},
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?v=\d+\.\d+\.\d+)?$/,
				include: path.resolve(__dirname, 'src/assets/images'),
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'images/',
						},
					},
				],
			},
			{
				test: /\.(svg)(\?v=\d+\.\d+\.\d+)?$/,
				include: path.resolve(__dirname, 'src/assets/inline'),
				use: [
					{
						loader: 'svg-inline-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'icons/',
						},
					},
				],
			},
			{
				test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/',
						},
					},
				],
			},
			{
				test: /\.m?js$/,
				exclude: /(node_modules|bower_components)/,
				use: {
					loader: 'babel-loader',
				},
			},
			{
				test: /\.(svelte)$/,
				// exclude: /(node_modules|public)/,
				use: {
					loader: 'svelte-loader',
					options: {
						emitCss: true,
						css: false,
						preprocess,
						hotReload: true,
					},
				},
			},
			{
				test: /\.(tsx?)$/,
				exclude: /(node_modules|public)/,
				use: {
					loader: 'ts-loader',
				},
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: path.join(__dirname, '/public/index.html'),
			inject: true,
		}),
		new MiniCssExtractPlugin({
			filename: '[name].css',
			chunkFilename: '[name].[id].css',
		}),
		new OptimizeCssAssetsPlugin({
			assetNameRegExp: /\.css$/g,
			cssProcessor: require('cssnano'),
			cssProcessorPluginOptions: {
				preset: ['default', { discardComments: { removeAll: true } }],
			},
			canPrint: true,
		}),
	],
};

const devServer = {
	devServer: {
		stats: 'errors-only',
	},
};

const devApi = {
	plugins: [
		new webpack.DefinePlugin({
			API_URL: JSON.stringify(process.env.API_URL),
			YA_API_KEY: JSON.stringify(process.env.YA_API_KEY),
		}),
	],
};

const prodApi = {
	plugins: [
		new webpack.DefinePlugin({
			API_URL: JSON.stringify(process.env.API_URL_PROD),
			YA_API_KEY: JSON.stringify(process.env.YA_API_KEY),
		}),
	],
};

module.exports = env => {
	if (env === 'production') {
		return merge([common, prodApi]);
	}
	if (env === 'development') {
		return merge([common, devServer, devApi]);
	}
};
