import { PROFILE } from '../../src/constants/routes';

context('Profile', () => {
	before(() => {
		cy.visit('');
		cy.window()
			.its('router')
			.then(store => store.setRoute(PROFILE));
	});

	describe('Registration', () => {
		it('Empty fields', () => {
			cy.contains('Want to Register?').click();
			cy.contains('Register').click();
			cy.get('.error').should(fields => {
				expect(fields).to.have.length(4);
				expect(fields.first()).to.contain('Fill this field');
			});
		});

		it('Incorrect phone and pass', () => {
			cy.contains('Name').within(() => {
				cy.get('input')
					.type('Valera')
					.should('have.value', 'Valera');
			});
			cy.contains('Phone number').within(() => {
				cy.get('input')
					.type('F')
					.should('have.value', 'F');
			});
			cy.contains('Password').within(() => {
				cy.get('input')
					.type('password')
					.should('have.value', 'password');
			});
			cy.contains('Repeat password').within(() => {
				cy.get('input')
					.type('stuff')
					.should('have.value', 'stuff');
			});
			cy.contains('Register').click();
			cy.get('.error').should(fields => {
				expect(fields).to.have.length(1);
				expect(fields.first()).to.contain('Enter valid phone number');
			});
		});

		it('Incorrect pass', () => {
			cy.contains('Phone number').within(() => {
				cy.get('input')
					.clear()
					.type('89994449944')
					.should('have.value', '89994449944');
			});
			cy.contains('Register').click();
			cy.get('.error').should(fields => {
				expect(fields).to.have.length(2);
				expect(fields.first()).to.contain(
					'Passwords should be identical'
				);
			});
		});

		it('Type correct credentials', () => {
			cy.contains('Repeat password').within(() => {
				cy.get('input')
					.clear()
					.type('password')
					.should('have.value', 'password');
			});
			cy.contains('Register').click();
			cy.contains('Phone number must be entered in the format');
		});
	});

	describe('Login', () => {
		it('Invalid credentials', () => {
			cy.contains('Want to Login?').click();
			cy.contains('Phone number').within(() => {
				cy.get('input')
					.clear()
					.type('+99999999999')
					.should('have.value', '+99999999999');
			});
			cy.contains('Password').within(() => {
				cy.get('input')
					.clear()
					.type('123')
					.should('have.value', '123');
			});
			cy.contains('Log in').click();
			cy.contains('Unable to log in with provided credentials.');
		});

		it('Valid credentials', () => {
			cy.contains('Phone number').within(() => {
				cy.get('input')
					.clear()
					.type('+123123123')
					.should('have.value', '+123123123');
			});
			cy.contains('Log in').click();
			cy.contains('Profile');
			cy.contains('hello, 123 with number +123123123, nice to meet you!');
			cy.saveLocalStorage();
		});

		it('Reload with saving state', () => {
			cy.restoreLocalStorage();
			cy.reload();
			cy.get('header').within(() => {
				cy.get('svg').click();
				cy.checkRoute(PROFILE);
			});
			cy.contains('hello, 123 with number +123123123, nice to meet you!');
		});

		it('User trips', () => {
			cy.contains('Your trips');
			cy.contains('Trip 1');
			cy.contains('From:');
			cy.contains('To:');
			cy.contains('Departing:');
		});

		it('Logout', () => {
			cy.contains('Log out').click();
			cy.contains('Login');
		});
	});
});
