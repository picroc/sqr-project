import { PASSENGERS, PROFILE, TRIPS } from '../../src/constants/routes';

context('Layout', () => {
	before(() => cy.visit(''));

	it('routes', () => {
		cy.get('header').within(() => {
			cy.contains('HackTheBus').click();
			cy.checkRoute(TRIPS);
			cy.contains('Trips').click();
			cy.checkRoute(TRIPS);
			cy.contains('Passengers').click();
			cy.checkRoute(PASSENGERS);
			cy.get('svg').click();
			cy.checkRoute(PROFILE);
		});
		cy.get('.dialog').within(() => {
			cy.get('.close').click();
			cy.checkRoute(TRIPS);
		});
	});
});

context('Last ride', () => {
	before(() => {
		cy.window()
			.its('router')
			.then(store => store.setRoute(PROFILE));

		cy.contains('Phone number').within(() => {
			cy.get('input')
				.clear()
				.type('+123123123')
				.should('have.value', '+123123123');
		});

		cy.contains('Password').within(() => {
			cy.get('input')
				.clear()
				.type('123')
				.should('have.value', '123');
		});

		cy.contains('Log in').click();
		cy.window()
			.its('router')
			.then(store => store.setRoute(TRIPS));
	});

	it('Last ride card', () => {
		cy.contains('Last ride');
	});
});
