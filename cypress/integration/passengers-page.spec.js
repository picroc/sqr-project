import { PASSENGERS, PROFILE } from '../../src/constants/routes';

context('Passengers', () => {
	before(() => {
		cy.clearLocalStorageSnapshot();
		cy.visit('');
		cy.window()
			.its('router')
			.then(store => store.setRoute(PASSENGERS));
	});

	beforeEach(() => {
		cy.restoreLocalStorage();
		cy.viewport(1920, 1080);
	});

	afterEach(() => {
		cy.saveLocalStorage();
	});

	describe('Request a trip', () => {
		it('Renders a "Request a trip" button', () => {
			cy.contains('Request a trip').should('be.visible');
		});

		it('Redirects to profile page on "Request a trip" button click if user is not logged in', () => {
			cy.contains('Request a trip').click();
			cy.contains('Log in');
		});

		it('Opens a "Request a trip" modal on "Request a trip" button click if user is logged in', () => {
			cy.window()
				.its('router')
				.then(store => store.setRoute(PROFILE));

			cy.contains('Log in');

			cy.contains('Phone number').within(() => {
				cy.get('input')
					.clear()
					.type('+78005553535');
			});

			cy.contains('Password').within(() => {
				cy.get('input')
					.clear()
					.type('ireknazm');
			});

			cy.contains('button', 'Log in').click();

			cy.contains('ireknazm').should('be.visible');

			cy.window()
				.its('router')
				.then(store => store.setRoute(PASSENGERS));

			cy.contains('Request a trip').click();
		});

		it('Shows errors while trying to submit the form inside Modal without filling it', () => {
			cy.get('.dialog').within(() => {
				cy.contains('button', 'Request').click();
			});

			cy.contains('Please, choose the start point').should('be.visible');
			cy.contains('Please, choose the destination point').should(
				'be.visible'
			);
			cy.contains('Please, pick the date and time').should('be.visible');
		});

		it('Creates a trip request if to fill the form completely and submit', () => {
			cy.contains('Start point')
				.parent()
				.parent()
				.within(() => {
					cy.get('input').type('Иннополис');
					cy.wait(1000);
					cy.contains('ymaps', 'Иннополис').click();
				});

			cy.contains('Destination point')
				.parent()
				.parent()
				.within(() => {
					cy.get('input').type('Казань');
					cy.wait(1000);
					cy.contains('ymaps', 'Казань').click();
				});

			cy.get('.date-picker').click();
			cy.get('.dropdown.calendar').within(() => {
				cy.contains('25').click();
			});

			cy.get('.time-picker').click();
			cy.contains('now').click();

			cy.get('.dialog').within(() => {
				cy.contains('button', 'Request').click();
			});

			cy.contains('Success!', { timeout: 2500 });
		});
	});
});
